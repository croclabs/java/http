package com.gitlab.croclabs.http.api;

import com.gitlab.croclabs.http.HttpTestHelper;
import com.gitlab.croclabs.http.api.javax.interfaces.ICrud;
import com.gitlab.croclabs.http.api.javax.request.Request;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

public class ApiTest extends HttpTestHelper {
	public static class TestImplJavax implements ICrud<String, String> {
		@Override
		public ResponseEntity<String> create(Request<String> request) {
			return ResponseEntity.ok("Okay");
		}

		@Override
		public ResponseEntity<String> read(Request<Void> request) {
			return ResponseEntity.ok("Okay");
		}

		@Override
		public ResponseEntity<String> update(Request<String> request) {
			return ResponseEntity.ok("Okay");
		}

		@Override
		public ResponseEntity<String> delete(Request<Void> request) {
			return ResponseEntity.ok("Okay");
		}
	}

	public static class TestImplJakarta implements com.gitlab.croclabs.http.api.jakarta.interfaces.ICrud<String, String> {

		@Override
		public ResponseEntity<String> create(com.gitlab.croclabs.http.api.jakarta.request.Request<String> request) {
			return ResponseEntity.ok("Okay");
		}

		@Override
		public ResponseEntity<String> read(com.gitlab.croclabs.http.api.jakarta.request.Request<Void> request) {
			return ResponseEntity.ok("Okay");
		}

		@Override
		public ResponseEntity<String> update(com.gitlab.croclabs.http.api.jakarta.request.Request<String> request) {
			return ResponseEntity.ok("Okay");
		}

		@Override
		public ResponseEntity<String> delete(com.gitlab.croclabs.http.api.jakarta.request.Request<Void> request) {
			return ResponseEntity.ok("Okay");
		}
	}

	@Test
	void test() {
		// Jakarta
		TestImplJakarta jakarta = new TestImplJakarta();

		assertRes(jakarta.read(mockReq, mockRes, mockSes, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
		assertRes(jakarta.delete(mockReq, mockRes, mockSes, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
		assertRes(jakarta.create(mockReq, mockRes, mockSes, "", new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
		assertRes(jakarta.update(mockReq, mockRes, mockSes, "", new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));

		// Javax
		TestImplJavax javax = new TestImplJavax();

		assertRes(javax.read(null, null, null, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
		assertRes(javax.delete(null, null, null,  new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
		assertRes(javax.create(null, null, null,  "", new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
		assertRes(javax.update(null, null, null,  "", new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>()));
	}

	private void assertRes(ResponseEntity<String> res) {
		Assertions.assertEquals("Okay", res.getBody());
	}
}
