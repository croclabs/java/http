package com.gitlab.croclabs.http;

import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class HttpTest {

	@Test
	void test() throws InterruptedException, MalformedURLException {
		Http.create("//");

		Http.create(new URL("https://www.google.com"))
				.client(client -> Logger.getAnonymousLogger().info(client.toString()))
				.request(request -> Logger.getAnonymousLogger().info(request.toString()))
				.send(BodyHandlers.ofString())
				.then(res -> assertNotNull(res.body()));

		Thread.sleep(5000);

		String str = Http.create("https://www.google.com")
				.send(BodyHandlers.ofString())
				.thenReturn(HttpResponse::body)
				.thenReturn(body -> {
					assertNotNull(body);
					return body;
				})
				.thenReturn();

		assertNotNull(str);
	}

}