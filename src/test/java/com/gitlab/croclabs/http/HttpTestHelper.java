package com.gitlab.croclabs.http;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;

public class HttpTestHelper {
	protected MockHttpServletRequest mockReq = new MockHttpServletRequest();
	protected MockHttpServletResponse mockRes = new MockHttpServletResponse();
	protected MockHttpSession mockSes = new MockHttpSession();
}
