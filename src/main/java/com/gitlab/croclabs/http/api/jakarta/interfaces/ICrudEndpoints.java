package com.gitlab.croclabs.http.api.jakarta.interfaces;

import com.gitlab.croclabs.http.api.jakarta.request.Request;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

public interface ICrudEndpoints {
	interface ICreate<O, B> {
		@PostMapping
		@ResponseBody
		default ResponseEntity<O> create(
				HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				@RequestBody B body,
				@PathVariable Map<String, String> pathVars,
				@RequestParam Map<String, String> params,
				@RequestHeader Map<String, String> headers,
				@RequestAttribute Map<String, Object> attrs
		) {
			return create(new Request<>(body)
					.request(request)
					.response(response)
					.session(session)
					.pathVars(pathVars)
					.params(params)
					.headers(headers)
					.attrs(attrs));
		}

		ResponseEntity<O> create(Request<B> request);
	}

	interface IRead<O> {
		@GetMapping
		@ResponseBody
		default ResponseEntity<O> read(
				HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				@PathVariable Map<String, String> pathVars,
				@RequestParam Map<String, String> params,
				@RequestHeader Map<String, String> headers,
				@RequestAttribute Map<String, Object> attrs
		) {
			return read(new Request<>()
					.request(request)
					.response(response)
					.session(session)
					.pathVars(pathVars)
					.params(params)
					.headers(headers)
					.attrs(attrs)
					.empty());
		}

		ResponseEntity<O> read(Request<Void> request);
	}

	interface IUpdate<O, B> {
		@PutMapping
		@ResponseBody
		default ResponseEntity<O> update(
				HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				@RequestBody B body,
				@PathVariable Map<String, String> pathVars,
				@RequestParam Map<String, String> params,
				@RequestHeader Map<String, String> headers,
				@RequestAttribute Map<String, Object> attrs
		) {
			return update(new Request<>(body)
					.request(request)
					.response(response)
					.session(session)
					.pathVars(pathVars)
					.params(params)
					.headers(headers)
					.attrs(attrs));
		}

		ResponseEntity<O> update(Request<B> request);
	}

	interface IDelete<O> {
		@DeleteMapping
		@ResponseBody
		default ResponseEntity<O> delete(
				HttpServletRequest request,
				HttpServletResponse response,
				HttpSession session,
				@PathVariable Map<String, String> pathVars,
				@RequestParam Map<String, String> params,
				@RequestHeader Map<String, String> headers,
				@RequestAttribute Map<String, Object> attrs
		) {
			return delete(new Request<>()
					.request(request)
					.response(response)
					.session(session)
					.pathVars(pathVars)
					.params(params)
					.headers(headers)
					.attrs(attrs)
					.empty());
		}

		ResponseEntity<O> delete(Request<Void> request);
	}
}
