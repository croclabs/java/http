package com.gitlab.croclabs.http.api.javax.request;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class Request<R> {
	R body;
	HttpServletRequest request;
	HttpServletResponse response;
	HttpSession session;
	Map<String, String> pathVars;
	Map<String, String> params;
	Map<String, String> headers;
	Map<String, Object> attrs;

	public Request() {
		super();
	}

	public Request(R body) {
		super();
		this.body = body;
	}

	@SuppressWarnings("unchecked")
	public Request<Void> empty() {
		return (Request<Void>) this;
	}

	public Request<R> request(HttpServletRequest request) {
		this.request = request;
		return this;
	}

	public Request<R> response(HttpServletResponse response) {
		this.response = response;
		return this;
	}

	public Request<R> session(HttpSession session) {
		this.session = session;
		return this;
	}

	public Request<R> pathVars(Map<String, String> pathVars) {
		this.pathVars = pathVars;
		return this;
	}

	public Request<R> params(Map<String, String> params) {
		this.params = params;
		return this;
	}

	public Request<R> headers(Map<String, String> headers) {
		this.headers = headers;
		return this;
	}

	public Request<R> attrs(Map<String, Object> attrs) {
		this.attrs = attrs;
		return this;
	}

	public R body() {
		return body;
	}

	public HttpServletRequest request() {
		return request;
	}

	public HttpServletResponse response() {
		return response;
	}

	public HttpSession session() {
		return session;
	}

	public Map<String, String> pathVars() {
		return pathVars;
	}

	public Map<String, String> params() {
		return params;
	}

	public Map<String, String> headers() {
		return headers;
	}

	public Map<String, Object> attrs() {
		return attrs;
	}
}
