package com.gitlab.croclabs.http.api.javax.interfaces;

import com.gitlab.croclabs.http.api.javax.interfaces.ICrudEndpoints.ICreate;
import com.gitlab.croclabs.http.api.javax.interfaces.ICrudEndpoints.IDelete;
import com.gitlab.croclabs.http.api.javax.interfaces.ICrudEndpoints.IRead;
import com.gitlab.croclabs.http.api.javax.interfaces.ICrudEndpoints.IUpdate;

public interface ICrud<O, B> extends ICreate<O, B>, IRead<O>, IUpdate<O, B>, IDelete<O> {
}
