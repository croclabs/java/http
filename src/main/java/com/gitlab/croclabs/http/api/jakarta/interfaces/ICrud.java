package com.gitlab.croclabs.http.api.jakarta.interfaces;

import com.gitlab.croclabs.http.api.jakarta.interfaces.ICrudEndpoints.ICreate;
import com.gitlab.croclabs.http.api.jakarta.interfaces.ICrudEndpoints.IDelete;
import com.gitlab.croclabs.http.api.jakarta.interfaces.ICrudEndpoints.IRead;
import com.gitlab.croclabs.http.api.jakarta.interfaces.ICrudEndpoints.IUpdate;

public interface ICrud<O, B> extends ICreate<O, B>, IRead<O>, IUpdate<O, B>, IDelete<O> {
}
